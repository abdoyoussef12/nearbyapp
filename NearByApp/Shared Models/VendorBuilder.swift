//
//  VendorBuilder.swift
//  NearByApp
//
//  Created by Apple on 10/9/21.
//

import Foundation
struct VendorBuilder{
    var name:String
    var address:String
    var imageLink:String?
    var id : String
}
