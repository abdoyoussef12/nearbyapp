//
//  LocationCoordinates.swift
//  NearByApp
//
//  Created by Apple on 10/9/21.
//


import Foundation

struct LocationCoordinates {
      var latitude : Double
      var longitude : Double
  }
