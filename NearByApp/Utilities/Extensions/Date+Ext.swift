//
//  Date+Ext.swift
//  NearByApp
//
//  Created by Apple on 10/9/21.
//

import Foundation
extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
