

// MARK: - Constants
enum Constants: String {
    case clientid = "3A5VXBCHILVZYFOCDW5QWLJWE3UUU4ZLLPKOY4JS1545GY12"
    case clientsecret = "5LPV5AZJMSLUYSKXR2TZ1BVVK1FQWUE1WCTP1DCCUQPRCI4Q"
}

// MARK: - RequestMethod
enum RequestMethod: String {
    case GET
    case POST
}
