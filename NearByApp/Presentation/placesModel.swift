//
//  placesModel.swift
//  NearByApp
//
//  Created by Apple on 10/10/21.
//

import RealmSwift

class Place: Object {
    
    @objc dynamic var name: String = ""
    @objc dynamic var address: String = ""
    @objc dynamic var id : String? = ""
    override class func primaryKey() -> String? {
           return "id"
       }
    convenience init(name: String,address:String,id:String) {
        self.init()
        self.name = name
        self.address = address
        self.id = id
    }
}
