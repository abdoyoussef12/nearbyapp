//
//  DataPresenter.swift
//  NearByApp
//
//  Created by Apple on 10/9/21.
//
import Foundation
import RealmSwift

protocol PlacesViewPresenter: class {
    func addItmes(place:[Place])
    func deleteItems()
}

class ItemsPresenter: PlacesViewPresenter {
    private   var PlaceResult: Results<Place>?
    private  var realm = try! Realm()
    
    func addItmes(place:[Place]) {
        addItem(place: place)
    }
    func deleteItems() {
        DeleteItems()
    }
    func retrieveItems(completion:@escaping([Place])->()) {
        self.PlaceResult = realm.objects(Place.self)
        
        let Places: [Place]? = self.PlaceResult?
            .compactMap { $0 }
        completion(Places ?? [])
    }
    
    private func addItem(place:[Place]) {
        if !realm.isInWriteTransaction{
            do {
                try? self.realm.write {
                    self.realm.add(place)
                }
            } catch {
                
            }
        }
        
    }
    private func DeleteItems() {
        do {
            let realm = try! Realm()
            let allItems =  realm.objects(Place.self)
            
            try! realm.write {
                realm.delete(allItems)
                try  realm.commitWrite()
            }
        } catch {
            
        }
    }
    
}
