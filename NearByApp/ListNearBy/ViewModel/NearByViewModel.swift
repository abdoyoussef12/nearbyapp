//
//  NearByViewModel.swift
//  NearByApp
//
//  Created by Apple on 10/8/21.
//

import Foundation
import RxSwift
import RxCocoa
import CoreLocation
import RxCoreLocation

class NearByViewModel {
    private var disposeBag = DisposeBag()
    var vendorsSubject = PublishSubject<[VendorBuilder]>()
    var imagesSubject = BehaviorSubject<String>(value: "")
    private var tempVendorsArray : [VendorBuilder] = []
    let showLoading = BehaviorRelay<Bool>(value: true)
    let showWaringError = BehaviorRelay<Bool>(value: true)
    let showEmptyPlaceError = BehaviorRelay<Bool>(value: true)
    private  var casheItems = PublishSubject<[Place]>()
    var placeList : [Place] = []
    let manager = CLLocationManager()

    // ARRAY OF SAVING DATA
    var vendorsObservable : Observable <[VendorBuilder]> {
        return vendorsSubject.asObservable()
    }
    var imagesObservel : Observable <String> {
        return imagesSubject
    }
    var casheItemsObservel : Observable <[Place]> {
        return casheItems
    }
    ///save call localmanger()
    var latestUpdatedImageRow = 0
    var isAllVendorHasImage:[Bool] = []
    private var imagesLinks : [String] = []
    private var tempHolderVendor:[VendorBuilder] = []
    let localPersistance = ItemsPresenter()
    
    
    func fetchData(latitude: Double, longitude: Double){
        showLoading.accept(true)
        NetworkService.shared.createRequest(decodedType: ExploreModel.self, urlPath: EndPoints.getPlaces(locationCoordinate: LocationCoordinates.init(latitude: latitude, longitude:  longitude)), method: .GET)
            .subscribe(onNext:{ [weak self] result in
                self?.showLoading.accept(false)
                self?.showWaringError.accept(true)
                guard let self = self else {return}
                let groups = result.response.groups[0].items
                if groups?.isEmpty == true {
                    self.showEmptyPlaceError.accept(false)
                    
                }else{
                    self.showEmptyPlaceError.accept(true)
                }
                
                let groupsSubject = BehaviorSubject<[GroupItem]>(value: [])
                groupsSubject.onNext(groups!)
                
                groupsSubject.subscribe { (groupItems) in
                    for (index,group) in groupItems.enumerated() {
                        self.isAllVendorHasImage.append(false)
                        var vendor = VendorBuilder(name: (group.venue?.name!)!, address: (group.venue?.location?.address) ?? "", imageLink: nil , id: (group.venue?.id!)!)
                        self.latestUpdatedImageRow = index
                        self.fetchDataImage(id:  (group.venue?.id!)!) { (imageLink) in
                            vendor.imageLink = imageLink
                            if   self.tempHolderVendor.count-1 >= index{
                                self.tempHolderVendor[index].imageLink = imageLink
                                self.imagesSubject.onNext(vendor.imageLink!)
                                self.isAllVendorHasImage[index] = true
                                let allsats =   self.isAllVendorHasImage.allSatisfy({$0})
                                if  (allsats){
                                    self.vendorsSubject.onNext(self.tempHolderVendor)
                                    self.tempHolderVendor = []
                                    self.isAllVendorHasImage = []
                                }
                            }
                            
                        }
                        self.tempVendorsArray.append(vendor)
                        
                    }
                } onCompleted: {
                    
                }.disposed(by: DisposeBag())
                
                self.vendorsSubject.onNext(self.tempVendorsArray)
                self.tempHolderVendor =  self.tempVendorsArray
                self.localPersistance.deleteItems()
                self.placeList = []
                
                for i in self.tempHolderVendor{
                    self.placeList.append(.init(name: i.name, address: i.address, id: i.id))
                }
                DispatchQueue.main.async {
                    self.localPersistance.addItmes(place: self.placeList)
                }
                self.tempVendorsArray = []
                // ARRAY OF GROUPS
            },onError: { error in
                self.showLoading.accept(false)
                self.showWaringError.accept(false)
                
            }).disposed(by: disposeBag)
    }
  
    
    func loadCashedItems(){
        DispatchQueue.main.async {
            self.localPersistance.retrieveItems { result in
                self.casheItems.onNext(result)
            }
        }
    }
    
    /// get image Palces
    /// - Parameters:
    ///   - id: uniqe image id
    ///   - completon:
    /// - Returns: link image
    func fetchDataImage(id:String,completon:@escaping (String)-> ()){
        NetworkService.shared.createRequest(decodedType: VenuePhotoModel.self, urlPath: EndPoints.getPhoto(id: id), method: .GET).subscribe(onNext:{ result in
            guard let items = result.response?.photos?.items , items.count > 0 else{
                completon("")
                return
            }
            let imageURlString = (items[0]!.prefix ?? "") + "\(items[0]!.height!)x\(items[0]!.width!)" + (items[0]!.suffix ?? "")
            completon( imageURlString )
        },onError: { error in

        }).disposed(by: disposeBag)
    }
    func saveImage(image: UIImage,imagePath:String) -> Bool {
        guard let data = image.jpegData(compressionQuality: 1) ?? image.pngData() else {
            return false
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return false
        }
        do {
            
            try data.write(to: directory.appendingPathComponent("\(imagePath).png")!)
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    func getSavedImage(named: String) -> UIImage? {
        
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
        }
        return nil
    }
}
extension NearByViewModel {
    /// Setup CLLocationManager
    
    func setupLocation(){
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
    ///Subscribing for a single location events
    func getLocation(){
        manager.rx
            .location
            .subscribe(onNext: { location in
                guard let location = location else { return }
                self.fetchData(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            })
            .disposed(by: disposeBag)
    }
    ///Subscribing for an array of location events
    func updateLocation(){
        self.manager.startUpdatingLocation()
        
        manager.rx
            .didUpdateLocations
            .subscribe(onNext: { _, locations in
                guard !locations.isEmpty,
                      let currentLocation = locations.last else { return }
                self.updateLocation(lat: currentLocation.coordinate.latitude, long: currentLocation.coordinate.longitude)
            })
            .disposed(by: disposeBag)
    }
    
    func stopLocation(){
        self.manager.stopUpdatingLocation()
    }
    func updateLocation(lat:Double,long:Double){
        manager.distanceFilter = 500
        manager.rx
            .distanceFilter
            .subscribe(onNext: { locations in                
            })
            .disposed(by: disposeBag)
    }
    ///Monitoring authorization changes
    func Monitoring() {
        manager.rx
            .didChangeAuthorization
            .subscribe(onNext: {_, status in
                switch status {
                case .denied:
                    print("Authorization denied")
                case .notDetermined:
                    print("Authorization: not determined")
                case .restricted:
                    print("Authorization: restricted")
                case .authorizedAlways, .authorizedWhenInUse:
                    print("All good fire request")
                @unknown default:
                    fatalError()
                }
            })
            .disposed(by: disposeBag)
    }
}


