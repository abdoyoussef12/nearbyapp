
//// MARK: - Encode
import Foundation

struct ExploreModel:Codable{
    let meta: Meta
    let response: Response
}

// MARK: - Meta
struct Meta: Codable {
    let code: Int?
    let requestID: String?
    enum CodingKeys: String, CodingKey {
        case code
        case requestID = "requestId"
    }
}



// MARK: - Response
struct Response: Codable{
    let suggestedRadius: Int?
    let headerLocation:String?
    let headerFullLocation:String?
    let headerLocationGranularity: String?
    let totalResults: Int?
    let groups: [Group]
}




// MARK: - Groups
struct Group:Codable {
    let items: [GroupItem]?
}
struct GroupItem: Codable{
    let venue: Venue?
}




// MARK: - Venue
struct Venue:Codable {
    let id:String?
    let name: String?
    let location: Location?
    let popularityByGeo: Double?
}
// MARK: - Location
struct Location: Codable {
    let address, crossStreet: String?
    let lat, lng: Double?
    let distance: Int?
    let postalCode, cc, city, state: String?
    let country: String?
    let formattedAddress: [String]?
    
}
