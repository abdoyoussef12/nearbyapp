//
//  EndPoint.swift
//  NearByApp
//
//  Created by Apple on 10/9/21.
//

import Foundation
enum  EndPoints {
    case getPlaces( locationCoordinate:LocationCoordinates)
    case getPhoto(id:String)
}
extension EndPoints {
    var path : String {
        switch self {
        case .getPlaces  :
            return "/v2/venues/explore"
        case .getPhoto(let id):
            return "/v2/venues/\(id)/photos"
        }
    }
    
    var queryParameters: [URLQueryItem] {
        switch self {
        case .getPlaces( let locationCoordinates):
            let letLongValue = "\(locationCoordinates.latitude),\(locationCoordinates.longitude)"
            let currentDate = getCurrentDate()
            
            return [
                URLQueryItem(name: "client_id", value:Constants.clientid.rawValue),
                URLQueryItem(name: "client_secret" ,value: Constants.clientsecret.rawValue),
                URLQueryItem(name: "ll", value: letLongValue),
                URLQueryItem(name: "v", value: currentDate),
                URLQueryItem(name: "radius", value: "1000"),
                URLQueryItem(name: "sortByDistance", value: "1")
            ]
        case .getPhoto:
            let currentDate = getCurrentDate()
            return [
                URLQueryItem(name: "client_id", value:Constants.clientid.rawValue),
                URLQueryItem(name: "client_secret" ,value: Constants.clientsecret.rawValue),
                URLQueryItem(name: "v", value: currentDate)
            ]
        }
    }
}
func getCurrentDate()-> String { return Date().string(format: "yyyyMMDD")  }
