//
//  NearByTableViewCell.swift
//  NearByApp
//
//  Created by Apple on 10/8/21.
//

import UIKit
class NearByTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageLocation: UIImageView!
    @IBOutlet weak var nameLocation: UILabel!
    @IBOutlet weak var addressLocation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
