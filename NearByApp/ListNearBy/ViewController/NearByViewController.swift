//
//  NearByViewController.swift
//  NearByApp
//
//  Created by Apple on 10/8/21.
//

import UIKit
import RxSwift
import SDWebImage
import RxCoreLocation
import CoreLocation
import Network
class NearByViewController: UIViewController {
    // MARK: - Properties
    let network = NetworkManager.sharedInstance
    
    var disposeBag = DisposeBag()
    @IBOutlet private weak var  tableView: UITableView!
    @IBOutlet  private weak var switchSegument: UISegmentedControl!
    @IBOutlet  private weak var viewError: UIView!
    @IBOutlet private weak var imageError: UIImageView!
    @IBOutlet  private weak var messgeError: UILabel!
    weak var presenter: PlacesViewPresenter!
    var viewModel = NearByViewModel()
    let loadingIndicator: ProgressViewes = {
        let progress = ProgressViewes(colors: [.red, .systemGreen, .systemBlue], lineWidth: 5)
        progress.translatesAutoresizingMaskIntoConstraints = false
        return progress
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.Monitoring()
        tableView.register(UINib(nibName: "\(NearByTableViewCell.self)", bundle: nil), forCellReuseIdentifier: "\(NearByTableViewCell.self)")
        checkConnection()
    }
    // MARK: - UI Setup
    private func setupUI() {
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        self.view.backgroundColor = .white
        self.view.addSubview(loadingIndicator)
        
        NSLayoutConstraint.activate([
            loadingIndicator.centerXAnchor
                .constraint(equalTo: self.view.centerXAnchor),
            loadingIndicator.centerYAnchor
                .constraint(equalTo: self.view.centerYAnchor),
            loadingIndicator.widthAnchor
                .constraint(equalToConstant: 50),
            loadingIndicator.heightAnchor
                .constraint(equalTo: self.loadingIndicator.widthAnchor)
        ])
    }

    func checkConnection(){
        network.reachability.whenReachable = { _ in
            DispatchQueue.main.async {
                self.tableView.delegate = nil
                self.tableView.dataSource = nil
            }
            self.fetchData()
            self.viewModel.setupLocation()
            DispatchQueue.main.async {
                self.viewModel.getLocation()
                self.viewModel.updateLocation()
                self.setupUI()
            }
            self.stoploadingIndicator()
            self.checkWaringMessage ()
            self.checkEmptyPlace()
            
        }
        
        network.reachability.whenUnreachable = { _ in
            DispatchQueue.main.async {
                self.tableView.delegate = nil
                self.tableView.dataSource = nil
            }
            DispatchQueue.main.async {
                self.subcribeLocal()
                
            }
            self.viewModel.loadCashedItems()
        }
    }
    
    func stoploadingIndicator(){
        viewModel.showLoading.asObservable().subscribe {  check in
            if check.element == true{
                self.loadingIndicator.isAnimating = true
                
            }else{
                DispatchQueue.main.async {
                    self.loadingIndicator.isAnimating = false
                }
                
            }
        }.disposed(by: disposeBag)
    }
    func checkWaringMessage (){
        viewModel.showWaringError.asObservable().subscribe {  check in
            if check.element == false{
                DispatchQueue.main.async {
                    self.viewError.isHidden = false
                    self.messgeError.text = "Something went wrong !! "
                    self.imageError.image = #imageLiteral(resourceName: "someThingWrong")
                }
            }else{
                DispatchQueue.main.async {
                    self.viewError.isHidden = true
                }
                
            }
        }.disposed(by: disposeBag)
    }
    func checkEmptyPlace (){
        viewModel.showEmptyPlaceError.asObservable().subscribe {  check in
            if check.element == false{
                DispatchQueue.main.async {
                    self.viewError.isHidden = false
                    self.messgeError.text = "Not Found Data !!"
                    self.imageError.image = #imageLiteral(resourceName: "search-engine")
                }
            }else{
                DispatchQueue.main.async {
                    self.viewError.isHidden = true
                }
                
            }
        }.disposed(by: disposeBag)
    }
    func fetchData(){
        DispatchQueue.main.async { [self] in
            self.tableView.rowHeight = UITableView.automaticDimension
            viewModel.vendorsObservable.bind( to: self.tableView.rx.items(cellIdentifier: "\(NearByTableViewCell.self)", cellType: NearByTableViewCell.self)) {
                row , service ,cell in
                cell.addressLocation.text = service.address
                cell.nameLocation.text = service.name
                guard let defultimage  = service.imageLink,defultimage != ""  else{
                    cell.imageLocation.image = #imageLiteral(resourceName: "imagePlaceholder")
                    return
                }
                cell.imageLocation?.sd_setImage(with: URL(string:defultimage))
                guard let image = cell.imageLocation.image else {
                    return
                }
                viewModel.saveImage(image:image, imagePath: service.id)
                
                
            }.disposed(by: self.disposeBag)
            
        }
        
    }
    func subcribeLocal(){
        tableView.rowHeight = UITableView.automaticDimension
        viewModel.casheItemsObservel.bind( to: tableView.rx.items(cellIdentifier: "\(NearByTableViewCell.self)", cellType: NearByTableViewCell.self)) {
            row , service ,cell in
            cell.addressLocation.text = service.address
            cell.nameLocation.text = service.name
            DispatchQueue.main.async {
                guard let image = self.viewModel.getSavedImage(named: service.id!) else{
                    cell.imageLocation.image = #imageLiteral(resourceName: "imagePlaceholder")
                    return
                }
                cell.imageLocation.image =   image
            }
        }.disposed(by: disposeBag)
    }
    @IBAction func swtichButton(_ sender: UISegmentedControl) {
        switch switchSegument.selectedSegmentIndex
        {
        case 0:
            viewModel.getLocation()
            viewModel.updateLocation()
        case 1:
            viewModel.getLocation()
            viewModel.stopLocation()
        default:
            break
        }
    }
}
