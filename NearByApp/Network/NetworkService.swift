
import RxSwift

class NetworkService: NSObject, NetworkManagerProtocol {
    //MARK: - Constants
    private let baseUrl = "api.foursquare.com"
    static let shared = NetworkService()
    
    //MARK: - CreateRequest
    func createRequest<T:Codable>(decodedType: T.Type, urlPath: EndPoints, method: RequestMethod)->Observable<T> {
        return Observable.create({[weak self] observer in
            
            var components = URLComponents()
            components.scheme = "https"
            components.host = self?.baseUrl
            components.path = urlPath.path
            components.queryItems = urlPath.queryParameters
                        
            guard let url = components.url else {return Disposables.create()}
            var urlRequest = URLRequest(url:  url)
            urlRequest.httpMethod = method.rawValue
            urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
            let session = URLSession(configuration: .default)
            
            let _ = session.dataTask(with: urlRequest) { [weak self] data, response, error in
                do {
                    guard let _ = self else {return}
                    guard let data = data else {
                        if let _ = error {
                            observer.onError(CustomNetworkError.invalidRequest("Can't get Network"))
                        }
                        return
                    }
                    let jsonData = try?  JSONDecoder().decode(decodedType, from: data)
                    
                    observer.onNext(jsonData!)
                 
                } catch  {
                    observer.onError(CustomNetworkError.decoding)
                }
            }.resume()
            return Disposables.create()
        })
    }
    
}

//MARK: - NetworkManagerProtocol
protocol NetworkManagerProtocol: AnyObject {
    func createRequest<T:Codable>(decodedType: T.Type,urlPath: EndPoints, method: RequestMethod)->Observable<T>
}


enum CustomNetworkError : Error{
    case invalidRequest(String)
    case decoding
}
